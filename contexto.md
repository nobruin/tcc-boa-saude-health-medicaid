# Contexto do sistema:

## Definição dos atores dentro do sistema:

`Operadora` objeto da analise: Boa Saúde.  
`Prestadores`: São os Colaboradores cadastrados médicos, enfermeiros, fisioterapeutas, dentistas e outros profissionais da área de saúde.  
`Conveniados`: São os laboratorios, hospitais e clinicas associados.  
`Associados`: São os clientes dos planos individuais e empresariais, eles estão dividos em tres categorias.
1) Ativos (com o plano em dia) 
2) Suspensos (com o plano em análise)
3) e Inativos (ex-clientes). 

Outra classificação dos clientes é por ``faixa``  etária, pois o valor da mensalidade do plano está diretamente vinculado à idade do
associado.

Também existe a divisão por tipo de plano, com três classificações: enfermaria, que é o plano mais básico com co-participação, apartamento, que é o plano intermediário sem co-participação e vip, que é o plano com mais serviços e atendimentos incluídos, sendo voltado para um público restrito (povo da grana).

O associado pode optar somente pelo plano médico ou pelo plano médico-odontológico. No segundo caso
existe um acréscimo de 15% no valor da mensalidade, exceto no plano vip (que inclui todo tipo de
atendimento). 

Visando aprimorar seus processos de gestão a empresa Boa Saúde Assistência Médica elaborou um Plano
de Metas e Diretrizes, que estabelece como prioridades para os próximos anos as seguintes ações na área
de tecnologia:
 Automatizar todos os tipos de atendimento realizados pela operadora de saúde, prestadores a ela
conveniados e associados, de forma integrada. Esta integração permitirá que tais atendimentos sejam
lançados em tempo real no sistema, visando passar pelos processos de apuração, conferência e
faturamento;
 Integrar os diferentes prestadores e conveniados, visando agilizar a tramitação de todos os
atendimentos;
 Utilizar geotecnologias em todos os procesos que envolvam localização, de forma a facilitar a
identificação e atualização de informações relativas aos atendimentos agendados e realizados;
 Tornar viável o uso de todas as tecnologias da informação e softwares necessários para atender às
demandas dos associados, gestores e prestadores, conforme definido.


## Lista básica dos requisitos a serem contemplados é apresentada a seguir:
- Possuir características de aplicação distribuída: abertura, portabilidade, uso de recursos de rede;
- Atender, de forma seletiva (por perfil) aos associados, cooperados e conveniados;
- Ser de fácil implantação e utilização;
- Ser hospedado em nuvem híbrida, sendo a forma de hospedagem documentada;
- Ser modular e implantável por módulos, de acordo com as prioridades e necessidades da empresa;
- Utilizar arquitetura baseada em serviços, total ou parcialmente;
- Suportar ambientes web e móveis;
- Possuir interface responsiva;
- Apresentar bom desempenho;
- Apresentar boa manutenibilidade;
- Ser testável em todas as suas funcionalidades;
- Ser recuperável (resiliente) no caso da ocorrência de erro;
- Utilizar APIs ou outros recursos adequados para integração;
- Estar disponível em horário integral (24 H), sete dias por semana;
- Ser desenvolvido utilizando recursos de gestão de configuração, com integração contínua.
OBS.: alguns itens aqui apresentados são expressões utilizadas pela área usuária. A definição real de cada
requisito a ser explicitado deve ser expressa de forma clara, objetiva e mensurável.

# O que deve ser entregue:
Espera-se que ao final do trabalho de conclusão de curso o aluno entregue os seguintes artefatos:  
1. Documento Modelo de Projeto Arquitetural totalmente preenchido em todas as suas seções, com
o nível de detalhamento adequado, conforme modelo fornecido.
2. Prova de conceito (protótipo arquitetural) da arquitetura, que contemple a implementação de pelo
menos três casos de uso / histórias de usuário críticos (de alta importância) para a arquitetura do
sistema. A escolha dessas funcionalidades deve ser feita pelo próprio aluno, ficando o professor à
disposição para eventuais dúvidas.